import unittest

import numpy as np

from cimem import pmf
from cimem import reconstruct_source_intensities
from cimem import solve
from cimem.core import Cluster
from cimem.core import GaussianPrior


class TestTemporalRegularizationProblem(unittest.TestCase):
    """Test the solver using a temporally regularized cluster problem"""

    def test_wide_forward(self):
        """Test using a wide forward operator"""

        # Always run the same test.
        np.random.seed(100)

        nb_sensors = 12
        nb_sources = 64
        nb_samples = 2

        # Generate the measurement using an identity matrix
        forward = np.random.randn(nb_sensors, nb_sources)
        source_intensities = 0.05 * np.random.randn(nb_sources, nb_samples)
        source_intensities[nb_sources // 2:, :] += 1
        measurements = np.dot(forward, source_intensities)

        # Two independent clusters.
        ones = np.ones((nb_sources // 2,))
        eye = np.eye(nb_sources // 2)
        cluster_1_forward = forward[:, :nb_sources // 2]
        priors_1 = (
            GaussianPrior(-ones, eye, cluster_1_forward),
            GaussianPrior(ones, eye, cluster_1_forward)
        )
        cluster_2_forward = forward[:, nb_sources // 2:]
        priors_2 = (
            GaussianPrior(-ones, eye, cluster_2_forward),
            GaussianPrior(ones, eye, cluster_2_forward)
        )
        sources = np.arange(nb_sources // 2)
        clusters = [Cluster('c1', sources, priors_1, s)
                    for s in range(nb_samples)]
        sources = np.arange(nb_sources // 2, nb_sources)
        clusters += [Cluster('c2', sources, priors_2, s)
                     for s in range(nb_samples)]

        # The clusters are in the same state at every sample.
        pmfs = []
        pmfs += [pmf(clusters[i:i + 2], [0.49, 0.01, 0.01, 0.49])
                 for i in range(nb_samples - 1)]
        pmfs += [pmf(clusters[i:i + 2], [0.49, 0.01, 0.01, 0.49])
                 for i in range(nb_samples, 2 * nb_samples - 1)]
        pmfs += [pmf([clusters[i], clusters[i + nb_samples]],
                     [0.01, 0.49, 0.49, 0.01])
                 for i in range(nb_samples)]

        lagrange, cost = solve(measurements, clusters, pmfs)
        recovered_intensities = reconstruct_source_intensities(
            cost, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

        # The normalization factor must be the same for all marginals.
        for cluster in clusters:
            self.assertAlmostEqual(
                cost.marginals[cluster].normalization,
                cost.marginals[clusters[0]].normalization, 5)

        # The measurements should be explained almost exactly.
        np.testing.assert_array_almost_equal(
            measurements, np.dot(forward, recovered_intensities), 3)

    def test_temporal_regularization(self):
        """Test using temporal regularization PMFs with 3 states"""

        # Always run the same test.
        np.random.seed(501)

        nb_sensors = 256
        nb_sources = 1024
        nb_samples = 8
        gradient_tolerance = 1e-4

        forward = np.random.rand(nb_sensors, nb_sources) / 10
        source_intensities = 0.05 * np.random.randn(nb_sources, nb_samples)
        source_intensities[nb_sources // 2:, nb_samples // 2:] += 1
        measurements = np.dot(forward, source_intensities)

        mean = 1.0
        variance = 1.0 * np.eye(nb_sources // 2)
        ones = np.ones((nb_sources // 2,))
        zeros = np.zeros((nb_sources // 2,))

        # Two independent clusters with 3 states.
        cluster_1_forward = forward[:, :nb_sources // 2]
        priors_cluster_1 = (
            GaussianPrior(-mean * ones, variance, cluster_1_forward),
            GaussianPrior(zeros, variance, cluster_1_forward),
            GaussianPrior(mean * ones, variance, cluster_1_forward)
        )
        cluster_2_forward = forward[:, nb_sources // 2:]
        priors_cluster_2 = (
            GaussianPrior(-mean * ones, variance, cluster_2_forward),
            GaussianPrior(zeros, variance, cluster_2_forward),
            GaussianPrior(mean * ones, variance, cluster_2_forward)
        )
        clusters = [Cluster('c1', np.arange(nb_sources // 2),
                            priors_cluster_1, s)
                    for s in range(nb_samples)]
        clusters += [Cluster('c2', np.arange(nb_sources // 2, nb_sources),
                     priors_cluster_2, s)
                     for s in range(nb_samples)]

        probabilities = np.array([2, 1, 0, 1, 2, 1, 0, 1, 2], dtype=np.float)
        probabilities /= probabilities.sum()
        pmfs = []
        pmfs += [pmf(clusters[i:i + 2], probabilities)
                 for i in range(nb_samples - 1)]
        pmfs += [pmf(clusters[i:i + 2], probabilities)
                 for i in range(nb_samples, 2 * nb_samples - 1)]

        lagrange, cost = solve(
            measurements, clusters, pmfs,
            gradient_tolerance=gradient_tolerance)
        recovered_sources_intensities = reconstruct_source_intensities(
            cost, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

        recovered_measurements = np.dot(forward, recovered_sources_intensities)
        np.testing.assert_array_almost_equal(
            measurements, recovered_measurements, 4)

        for cluster in clusters[nb_samples:]:
            probabilities = cost.marginals[cluster].probabilities
            if cluster.sample > nb_samples // 2:
                self.assertTrue(probabilities[2] > 0.9)
