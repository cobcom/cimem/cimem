import logging
import unittest

import numpy as np
import scipy.sparse

import cimem
import cimem.core
from bayesnet import DiscreteRandomVariable
from cimem import pmf


# When running tests, prevent logging information.
cimem.logger.setLevel(logging.CRITICAL)


class TestSolve(unittest.TestCase):
    """Test the cimem.solve function"""

    def test_one_cluster_one_source(self):
        """Test using a single cluster with a single source"""

        nb_samples = 1
        source_intensities = np.full((nb_samples, 1), 1.5)

        # The model is a single cluster with one source.
        forward = np.eye(1)
        priors = (
            cimem.core.GaussianPrior(np.array([0]), np.array([[.1]]), forward),
            cimem.core.GaussianPrior(np.array([1]), np.array([[.1]]), forward)
        )
        clusters = [cimem.core.Cluster('A', [0], priors, 0)]

        # Solve the MEM problem and reconstruct the source intensity.
        lagrange, marginals = cimem.solve(source_intensities, clusters)
        intensities = cimem.reconstruct_source_intensities(
            marginals, clusters, 1, 1, nb_samples, lagrange)
        np.testing.assert_array_almost_equal(
            intensities, source_intensities, 4)

    def test_one_cluster_two_sources(self):
        """Test using single cluster with two sources"""

        nb_sensors = 2
        nb_sources = 2
        nb_samples = 1
        source_intensities = np.full((nb_sources, nb_samples), 1.0)

        # The model is a single cluster with two sources.
        forward = np.eye(nb_sensors)
        priors = (
            cimem.core.GaussianPrior(
                np.array([0, 0]), 0.1 * np.eye(2), forward),
            cimem.core.GaussianPrior(
                np.array([1, 1]), 0.1 * np.eye(2), forward)
        )
        clusters = [cimem.core.Cluster('A', [0, 1], priors, 0)]

        # Solve the MEM problem and reconstruct the source intensities.
        lagrange, marginals = cimem.solve(source_intensities, clusters)
        intensities = cimem.reconstruct_source_intensities(
            marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)
        np.testing.assert_array_almost_equal(intensities, source_intensities)

    def test_one_cluster_with_forward(self):
        """Test using a single cluster with two sources but one observation"""

        nb_sensors = 1
        nb_sources = 2
        nb_samples = 1
        source_intensities = np.full((nb_sources, nb_samples), 1.0)
        forward = np.array([[1.0, 1.0]])
        data = np.dot(forward, source_intensities)

        # The model is a single cluster with two sources.
        priors = (
            cimem.core.GaussianPrior([0, 0], 0.1 * np.eye(2), forward),
            cimem.core.GaussianPrior([1, 1], 0.1 * np.eye(2), forward)
        )
        clusters = [cimem.core.Cluster('A', [0, 1], priors, 0)]

        # Solve the MEM problem and reconstruct the source intensity.
        lagrange, marginals = cimem.solve(data, clusters)
        intensities = cimem.reconstruct_source_intensities(
            marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)
        np.testing.assert_array_almost_equal(intensities, source_intensities)

    def test_two_independent_clusters(self):
        """Test using two independent clusters"""

        nb_sensors = 2
        nb_sources = 2
        nb_samples = 1
        source_intensities = np.array([[-1.0], [2.0]])
        forward = np.eye(nb_sources)
        data = np.dot(forward, source_intensities)

        # The model is a two clusters with a single source each.
        priors_cluster_1 = (
            cimem.core.GaussianPrior([0], 1 * np.eye(1), forward[:, 0:1]),
            cimem.core.GaussianPrior([-1], 1 * np.eye(1), forward[:, 0:1])
        )
        priors_cluster_2 = (
            cimem.core.GaussianPrior([0], 1 * np.eye(1), forward[:, 1:]),
            cimem.core.GaussianPrior([2], 1 * np.eye(1), forward[:, 1:])
        )
        clusters = [
            cimem.core.Cluster('A', [0], priors_cluster_1, 0),
            cimem.core.Cluster('B', [1], priors_cluster_2, 0)
        ]

        # Solve the MEM problem and reconstruct the source intensity.
        lagrange, marginals = cimem.solve(data, clusters)
        intensities = cimem.reconstruct_source_intensities(
            marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)
        np.testing.assert_array_almost_equal(
            intensities, source_intensities, 5)

    def test_with_two_time_samples(self):
        """Test using two time samples"""

        nb_sensors = 1
        nb_sources = 1
        nb_samples = 2
        source_intensities = np.array([[0.0, 1.0]])  # One source two times.
        forward = np.eye(nb_sources)
        data = np.dot(forward, source_intensities)

        # The model is two clusters, one for each sample.
        priors_cluster_1 = (
            cimem.core.GaussianPrior([0], 0.1 * np.eye(1), forward),
            cimem.core.GaussianPrior([1], 0.1 * np.eye(1), forward)
        )
        priors_cluster_2 = (
            cimem.core.GaussianPrior([0], 0.1 * np.eye(1), forward),
            cimem.core.GaussianPrior([1], 0.1 * np.eye(1), forward)
        )
        clusters = [
            cimem.core.Cluster('A', [0], priors_cluster_1, 0),
            cimem.core.Cluster('B', [0], priors_cluster_2, 1)
        ]

        # Solve the MEM problem and reconstruct the source intensity.
        lagrange, marginals = cimem.solve(data, clusters)
        intensities = cimem.reconstruct_source_intensities(
            marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)
        np.testing.assert_array_almost_equal(intensities, source_intensities)

    def test_solve_vs_pinv(self):
        """Test the solve method by comparing it to the pseudoinverse"""

        # Always run the same test.
        np.random.seed(200)

        nb_sensors = 32
        nb_sources = 32
        nb_samples = 1

        # Build a simple problem.
        forward = np.random.randn(nb_sensors, nb_sources)
        source_intensities = np.random.randn(nb_sources, nb_samples)
        source_intensities[nb_sources // 2:] = 0
        measurements = np.dot(forward, source_intensities)

        # A single Gaussian prior for all sources. This makes to solution
        # equal to the minimum norm solution.
        priors = cimem.core.GaussianPrior(
            np.zeros((nb_sources,)), np.eye(nb_sources), forward),
        clusters = [
            cimem.core.Cluster('all-sources', range(nb_sources), priors, 0)]

        lagrange, marginals = cimem.solve(
            measurements, clusters, gradient_tolerance=1e-7)
        recovered_cimem = cimem.reconstruct_source_intensities(
            marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

        # The pseudo inverse solution.
        pseudoinverse = np.linalg.pinv(forward)
        recovered_pinv = np.dot(pseudoinverse, measurements)

        # The solution should fit the measurements.
        np.testing.assert_array_almost_equal(
            measurements, np.dot(forward, recovered_cimem), 4)

        # The solutions should be almost identical.
        np.testing.assert_array_almost_equal(
            recovered_cimem, recovered_pinv, 4)

    def test_add_pmfs(self):
        """Test using additional pmfs"""

        # Always run the same test.
        np.random.seed(300)

        nb_sensors = 32
        nb_sources = 32
        nb_samples = 1

        # Build a simple problem.
        forward = np.random.randn(nb_sensors, nb_sources)
        source_intensities = np.random.randn(nb_sources, nb_samples)
        measurements = np.dot(forward, source_intensities)

        # A single Gaussian prior for all sources. This makes to solution
        # equal to the minimum norm solution.
        priors = (
            cimem.core.GaussianPrior(np.zeros((nb_sources,)),
                                     np.eye(nb_sources),
                                     forward),
            cimem.core.GaussianPrior(np.ones((nb_sources,)),
                                     np.eye(nb_sources), forward)
        )
        clusters = [
            cimem.core.Cluster('all-sources', range(nb_sources), priors, 0)]

        # Add pmfs which forces activity.
        pmfs = [pmf(clusters, [0.00, 1.00])]

        lagrange, cost = cimem.solve(
            measurements, clusters, pmfs, gradient_tolerance=1e-6)
        recovered_cimem = cimem.reconstruct_source_intensities(
            cost, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

        # The solution should fit the measurements.
        np.testing.assert_array_almost_equal(
            measurements, np.dot(forward, recovered_cimem), 2)

        # The cluster must be fully active.
        np.testing.assert_array_almost_equal(
            cost.marginals[clusters[0]].probabilities, [0.0, 1.0])

    def test_sparse_variance(self):
        """Test using a sparse matrix for the variance."""

        nb_sensors = 32
        nb_sources = 64
        nb_samples = 1

        # Build a simple problem.
        forward = np.random.randn(nb_sensors, nb_sources)
        source_intensities = np.random.randn(nb_sources, nb_samples)
        source_intensities[nb_sources // 2:] = 0
        measurements = np.dot(forward, source_intensities)

        # A single Gaussian prior for all sources. This makes to solution
        # equal to the minimum norm solution.
        priors = (
            cimem.core.GaussianPrior(np.zeros((nb_sources,)),
                                     np.zeros((nb_sources, nb_sources)),
                                     forward),
            cimem.core.GaussianPrior(np.zeros((nb_sources,)),
                                     np.eye(nb_sources), forward)
        )
        clusters = [
            cimem.core.Cluster('all-sources', range(nb_sources), priors, 0)]

        lagrange, marginals = cimem.solve(
            measurements, clusters, gradient_tolerance=1e-5)
        recovered_cimem = cimem.reconstruct_source_intensities(
            marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

        # Use the same setup, but with sparse variance matrices.
        priors = (
            cimem.core.GaussianPrior(
                np.zeros((nb_sources,)),
                scipy.sparse.csr_matrix(
                    (nb_sources, nb_sources), dtype=np.float),
                forward),
            cimem.core.GaussianPrior(
                np.zeros((nb_sources,)),
                scipy.sparse.identity(nb_sources, dtype=np.float), forward)
        )
        clusters = [
            cimem.core.Cluster('all-sources', range(nb_sources), priors, 0)]

        lagrange, marginals = cimem.solve(
            measurements, clusters, gradient_tolerance=1e-5)
        sparse_recovered_cimem = cimem.reconstruct_source_intensities(
            marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

        # Using sparse matrices or not, the result should be the same.
        np.testing.assert_array_almost_equal(
            sparse_recovered_cimem, recovered_cimem)


class TestCost(unittest.TestCase):
    """Test the cost function that is minimized in CIMEM"""

    def test_zero_data(self):
        """Test the case for all zero data"""

        nb_sensors = 32
        nb_sources = 64
        nb_samples = 1

        # Build a simple problem.
        forward = np.random.randn(nb_sensors, nb_sources)
        source_intensities = np.zeros((nb_sources, nb_samples))
        measurements = np.dot(forward, source_intensities)

        # A single Gaussian prior for all sources. This makes to solution
        # equal to the minimum norm solution.
        mean = np.ones((nb_sources,))
        variance = np.eye(nb_sources)
        priors = cimem.core.GaussianPrior(mean, variance, forward),
        clusters = [
            cimem.core.Cluster('all-sources', range(nb_sources), priors, 0)]

        lagrange, marginals = cimem.solve(
            measurements, clusters, gradient_tolerance=1e-7)

        # The gradient at the optimal solution should be close to zero.
        covariance = scipy.sparse.csr_matrix(
            (measurements.size, measurements.size), dtype=np.float)
        cost = cimem.Cost(measurements, clusters, [], covariance)
        value = cost(lagrange)
        gradient = cost.jac(lagrange)
        np.testing.assert_array_almost_equal(
            gradient, np.zeros_like(gradient), 4)

        # Check the value of the cost.
        value = cost(np.zeros_like(lagrange))
        gradient = cost.jac(np.zeros_like(lagrange))
        self.assertAlmostEqual(value, 0.0)
        np.testing.assert_array_almost_equal(gradient, np.dot(forward, mean))

        lagrange = np.ones_like(lagrange) * 0.1
        value = cost(lagrange)
        gradient = cost.jac(lagrange)
        expected_value = \
            0.5 * lagrange.dot(forward).dot(variance).dot(forward.T).dot(lagrange) + \
            lagrange.dot(forward).dot(mean)
        self.assertAlmostEqual(expected_value, value)

        expected_gradient = \
            lagrange.dot(forward).dot(variance).dot(forward.T) + \
            forward.dot(mean)
        np.testing.assert_array_almost_equal(expected_gradient, gradient)

    def test_derivatives_2_clusters_1_connection(self):
        """Test the derivatives of the cost for a simple setup"""

        priors = [
            cimem.core.GaussianPrior(np.zeros((2,)), np.eye(2), np.eye(2)),
            cimem.core.GaussianPrior(np.ones((2,)), np.eye(2), np.eye(2)),
        ]
        clusters = [
            cimem.core.Cluster('all-sources', [0, 1], priors, 0),
            cimem.core.Cluster('all-sources', [2, 3], priors, 1),
        ]

        lagrange = np.array([0.25, 0.10, 0.05, 0.01])

        connection = DiscreteRandomVariable('C', (0, 1))
        pmfs = [
            cimem.pmf([connection], [0.25, 0.75]),
            cimem.pmf([connection, clusters[0]], [0.45, 0.05, 0.05, 0.45]),
            cimem.pmf([connection, clusters[1]], [0.45, 0.05, 0.05, 0.45]),
        ]

        # Compute the Jacobian using the solve function.
        covariance = scipy.sparse.csr_matrix((4, 4), dtype=np.float)
        cost = cimem.Cost(np.zeros((2, 2)), clusters, pmfs, covariance)
        value = cost(lagrange)
        jac = cost.jac(lagrange)

        # Compute the expected Jacobian.
        ell = lagrange[:2]
        e00 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de00 = np.r_[ell, 0, 0]
        e01 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de01 = np.r_[ell + np.ones((2,)), 0, 0]
        ell = lagrange[2:]
        e10 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de10 = np.r_[0, 0, ell]
        e11 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de11 = np.r_[0, 0, ell + np.ones((2,))]

        expected_value = \
            0.25 * 0.45 * 0.45 * e00 * e10 + \
            0.25 * 0.45 * 0.05 * e00 * e11 + \
            0.25 * 0.05 * 0.45 * e01 * e10 + \
            0.25 * 0.05 * 0.05 * e01 * e11 + \
            0.75 * 0.05 * 0.05 * e00 * e10 + \
            0.75 * 0.05 * 0.45 * e00 * e11 + \
            0.75 * 0.45 * 0.05 * e01 * e10 + \
            0.75 * 0.45 * 0.45 * e01 * e11

        expected_jac = (
            0.25 * 0.45 * 0.45 * e00 * e10 * (de00 + de10) +
            0.25 * 0.45 * 0.05 * e00 * e11 * (de00 + de11) +
            0.25 * 0.05 * 0.45 * e01 * e10 * (de01 + de10) +
            0.25 * 0.05 * 0.05 * e01 * e11 * (de01 + de11) +
            0.75 * 0.05 * 0.05 * e00 * e10 * (de00 + de10) +
            0.75 * 0.05 * 0.45 * e00 * e11 * (de00 + de11) +
            0.75 * 0.45 * 0.05 * e01 * e10 * (de01 + de10) +
            0.75 * 0.45 * 0.45 * e01 * e11 * (de01 + de11)
        ) / expected_value

        def outer(v):
            return np.outer(v, v)

        np.testing.assert_array_almost_equal(jac, expected_jac)

    def test_derivatives_1_cluster(self):
        """Test the derivatives of the cost for a single cluster"""

        priors = [
            cimem.core.GaussianPrior(np.zeros((2,)), np.eye(2), np.eye(2)),
            cimem.core.GaussianPrior(np.ones((2,)), np.eye(2), np.eye(2)),
        ]
        clusters = [
            cimem.core.Cluster('all-sources', [0, 1], priors, 0),
        ]

        lagrange = np.array([0.1, 0.20])

        # Compute the Jacobian using the solve function.
        covariance = scipy.sparse.csr_matrix((2, 2), dtype=np.float)
        cost = cimem.Cost(np.zeros((2, 1)), clusters, [], covariance)
        value = cost(lagrange)
        jac = cost.jac(lagrange)

        # Compute the expected Jacobian.
        ell = lagrange
        e00 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de00 = ell
        e01 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de01 = ell + np.ones((2,))

        expected_value = 0.5 * e00 + 0.5 * e01

        expected_jac = (0.5 * e00 * de00 + 0.5 * e01 * de01) / expected_value

        np.testing.assert_array_almost_equal(jac, expected_jac)


    def test_derivatives_2_independent_clusters(self):
        """Test the derivatives of the cost for a single cluster"""

        priors = [
            cimem.core.GaussianPrior(np.zeros((2,)), np.eye(2), np.eye(2)),
            cimem.core.GaussianPrior(np.ones((2,)), np.eye(2), np.eye(2)),
        ]
        clusters = [
            cimem.core.Cluster('all-sources', [0, 1], priors, 0),
            cimem.core.Cluster('all-sources', [2, 3], priors, 1),
        ]

        lagrange = np.array([0.1, 0.2, 0.3, 0.4])

        # Compute the Jacobian using the solve function.
        covariance = scipy.sparse.csr_matrix((4, 4), dtype=np.float)
        cost = cimem.Cost(np.zeros((2, 2)), clusters, [], covariance)
        value = cost(lagrange)
        jac = cost.jac(lagrange)

        # Compute the expected Jacobian.
        ell = lagrange[:2]
        e00 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de00 = np.r_[ell, 0, 0]
        dde00 = np.zeros((4, 4))
        dde00[0, 0] = 1
        dde00[1, 1] = 1
        e01 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de01 = np.r_[ell + np.ones((2,)), 0, 0]
        dde01 = dde00
        ell = lagrange[2:]
        e10 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de10 = np.r_[0, 0, ell]
        dde10 = np.zeros((4, 4))
        dde10[2, 2] = 1
        dde10[3, 3] = 1
        e11 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de11 = np.r_[0, 0, ell + np.ones((2,))]
        dde11 = dde10

        expected_value = (0.5 * e00 + 0.5 * e01) * (0.5 * e10 + 0.5 * e11)

        expected_jac = (
            (0.5 * e00 * de00 + 0.5 * e01 * de01) * (0.5 * e10 + 0.5 * e11) +
            (0.5 * e10 * de10 + 0.5 * e11 * de11) * (0.5 * e00 + 0.5 * e01)
        ) / expected_value

        np.testing.assert_array_almost_equal(jac, expected_jac)

    def test_derivatives_3_clusters_2_connection(self):
        """Test the derivatives of the cost for a simple setup"""

        priors = [
            cimem.core.GaussianPrior(np.zeros((2,)), np.eye(2), np.eye(2)),
            cimem.core.GaussianPrior(np.ones((2,)), np.eye(2), np.eye(2)),
        ]
        clusters = [
            cimem.core.Cluster('S0', [0, 1], priors, 0),
            cimem.core.Cluster('S1', [2, 3], priors, 1),
            cimem.core.Cluster('S2', [4, 5], priors, 2),
        ]

        lagrange = np.array([0.25, 0.10, 0.05, 0.01, 0.3, 0.2])

        connection0 = DiscreteRandomVariable('C0', (0, 1))
        connection1 = DiscreteRandomVariable('C1', (0, 1))
        pmfs = [
            cimem.pmf([connection0], [0.25, 0.75]),
            cimem.pmf([connection0, clusters[0]], [0.45, 0.05, 0.05, 0.45]),
            cimem.pmf([connection0, clusters[1]], [0.45, 0.05, 0.05, 0.45]),
            cimem.pmf([connection1], [0.25, 0.75]),
            cimem.pmf([connection1, clusters[1]], [0.45, 0.05, 0.05, 0.45]),
            cimem.pmf([connection1, clusters[2]], [0.45, 0.05, 0.05, 0.45]),
        ]

        # Compute the Jacobian using the solve function.
        covariance = scipy.sparse.csr_matrix((6, 6), dtype=np.float)
        cost = cimem.Cost(np.zeros((2, 3)), clusters, pmfs, covariance)
        value = cost(lagrange)
        jac = cost.jac(lagrange)

        # Compute the expected Jacobian.
        ell = lagrange[:2]
        e00 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de00 = np.r_[ell, 0, 0, 0, 0]
        e01 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de01 = np.r_[ell + np.ones((2,)), 0, 0, 0, 0]
        ell = lagrange[2:4]
        e10 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de10 = np.r_[0, 0, ell, 0, 0]
        e11 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de11 = np.r_[0, 0, ell + np.ones((2,)), 0, 0]
        ell = lagrange[4:]
        e20 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.zeros((2,))))
        de20 = np.r_[0, 0, 0, 0, ell]
        e21 = np.exp(0.5 * np.dot(ell, ell) + np.dot(ell, np.ones((2,))))
        de21 = np.r_[0, 0, 0, 0, ell + np.ones((2,))]

        expected_value = (
            0.25 * 0.25 * 0.45 * 0.45 * 0.45 * 0.45 * e00 * e10 * e20 +  # 0, 0, 0, 0, 0
            0.25 * 0.25 * 0.45 * 0.45 * 0.45 * 0.05 * e00 * e10 * e21 +  # 0, 0, 0, 0, 1
            0.25 * 0.25 * 0.45 * 0.05 * 0.05 * 0.45 * e00 * e11 * e20 +  # 0, 0, 0, 1, 0
            0.25 * 0.25 * 0.45 * 0.05 * 0.05 * 0.05 * e00 * e11 * e21 +  # 0, 0, 0, 1, 1
            0.25 * 0.25 * 0.05 * 0.45 * 0.45 * 0.45 * e01 * e10 * e20 +  # 0, 0, 1, 0, 0
            0.25 * 0.25 * 0.05 * 0.45 * 0.45 * 0.05 * e01 * e10 * e21 +  # 0, 0, 1, 0, 1
            0.25 * 0.25 * 0.05 * 0.05 * 0.05 * 0.45 * e01 * e11 * e20 +  # 0, 0, 1, 1, 0
            0.25 * 0.25 * 0.05 * 0.05 * 0.05 * 0.05 * e01 * e11 * e21 +  # 0, 0, 1, 1, 1

            0.25 * 0.75 * 0.45 * 0.45 * 0.05 * 0.05 * e00 * e10 * e20 +  # 0, 1, 0, 0, 0
            0.25 * 0.75 * 0.45 * 0.45 * 0.05 * 0.45 * e00 * e10 * e21 +  # 0, 1, 0, 0, 1
            0.25 * 0.75 * 0.45 * 0.05 * 0.45 * 0.05 * e00 * e11 * e20 +  # 0, 1, 0, 1, 0
            0.25 * 0.75 * 0.45 * 0.05 * 0.45 * 0.45 * e00 * e11 * e21 +  # 0, 1, 0, 1, 1
            0.25 * 0.75 * 0.05 * 0.45 * 0.05 * 0.05 * e01 * e10 * e20 +  # 0, 1, 1, 0, 0
            0.25 * 0.75 * 0.05 * 0.45 * 0.05 * 0.45 * e01 * e10 * e21 +  # 0, 1, 1, 0, 1
            0.25 * 0.75 * 0.05 * 0.05 * 0.45 * 0.05 * e01 * e11 * e20 +  # 0, 1, 1, 1, 0
            0.25 * 0.75 * 0.05 * 0.05 * 0.45 * 0.45 * e01 * e11 * e21 +  # 0, 1, 1, 1, 1

            0.75 * 0.25 * 0.05 * 0.05 * 0.45 * 0.45 * e00 * e10 * e20 +  # 1, 0, 0, 0, 0
            0.75 * 0.25 * 0.05 * 0.05 * 0.45 * 0.05 * e00 * e10 * e21 +  # 1, 0, 0, 0, 1
            0.75 * 0.25 * 0.05 * 0.45 * 0.05 * 0.45 * e00 * e11 * e20 +  # 1, 0, 0, 1, 0
            0.75 * 0.25 * 0.05 * 0.45 * 0.05 * 0.05 * e00 * e11 * e21 +  # 1, 0, 0, 1, 1
            0.75 * 0.25 * 0.45 * 0.05 * 0.45 * 0.45 * e01 * e10 * e20 +  # 1, 0, 1, 0, 0
            0.75 * 0.25 * 0.45 * 0.05 * 0.45 * 0.05 * e01 * e10 * e21 +  # 1, 0, 1, 0, 1
            0.75 * 0.25 * 0.45 * 0.45 * 0.05 * 0.45 * e01 * e11 * e20 +  # 1, 0, 1, 1, 0
            0.75 * 0.25 * 0.45 * 0.45 * 0.05 * 0.05 * e01 * e11 * e21 +  # 1, 0, 1, 1, 1

            0.75 * 0.75 * 0.05 * 0.05 * 0.05 * 0.05 * e00 * e10 * e20 +  # 1, 1, 0, 0, 0
            0.75 * 0.75 * 0.05 * 0.05 * 0.05 * 0.45 * e00 * e10 * e21 +  # 1, 1, 0, 0, 1
            0.75 * 0.75 * 0.05 * 0.45 * 0.45 * 0.05 * e00 * e11 * e20 +  # 1, 1, 0, 1, 0
            0.75 * 0.75 * 0.05 * 0.45 * 0.45 * 0.45 * e00 * e11 * e21 +  # 1, 1, 0, 1, 1
            0.75 * 0.75 * 0.45 * 0.05 * 0.05 * 0.05 * e01 * e10 * e20 +  # 1, 1, 1, 0, 0
            0.75 * 0.75 * 0.45 * 0.05 * 0.05 * 0.45 * e01 * e10 * e21 +  # 1, 1, 1, 0, 1
            0.75 * 0.75 * 0.45 * 0.45 * 0.45 * 0.05 * e01 * e11 * e20 +  # 1, 1, 1, 1, 0
            0.75 * 0.75 * 0.45 * 0.45 * 0.45 * 0.45 * e01 * e11 * e21    # 1, 1, 1, 1, 1
        )
        self.assertAlmostEqual(value, np.log(expected_value))

        expected_jac = (
            0.25 * 0.25 * 0.45 * 0.45 * 0.45 * 0.45 * e00 * e10 * e20 * (de00 + de10 + de20) +  # 0, 0, 0, 0, 0
            0.25 * 0.25 * 0.45 * 0.45 * 0.45 * 0.05 * e00 * e10 * e21 * (de00 + de10 + de21) +  # 0, 0, 0, 0, 1
            0.25 * 0.25 * 0.45 * 0.05 * 0.05 * 0.45 * e00 * e11 * e20 * (de00 + de11 + de20) +  # 0, 0, 0, 1, 0
            0.25 * 0.25 * 0.45 * 0.05 * 0.05 * 0.05 * e00 * e11 * e21 * (de00 + de11 + de21) +  # 0, 0, 0, 1, 1
            0.25 * 0.25 * 0.05 * 0.45 * 0.45 * 0.45 * e01 * e10 * e20 * (de01 + de10 + de20) +  # 0, 0, 1, 0, 0
            0.25 * 0.25 * 0.05 * 0.45 * 0.45 * 0.05 * e01 * e10 * e21 * (de01 + de10 + de21) +  # 0, 0, 1, 0, 1
            0.25 * 0.25 * 0.05 * 0.05 * 0.05 * 0.45 * e01 * e11 * e20 * (de01 + de11 + de20) +  # 0, 0, 1, 1, 0
            0.25 * 0.25 * 0.05 * 0.05 * 0.05 * 0.05 * e01 * e11 * e21 * (de01 + de11 + de21) +  # 0, 0, 1, 1, 1

            0.25 * 0.75 * 0.45 * 0.45 * 0.05 * 0.05 * e00 * e10 * e20 * (de00 + de10 + de20) +  # 0, 1, 0, 0, 0
            0.25 * 0.75 * 0.45 * 0.45 * 0.05 * 0.45 * e00 * e10 * e21 * (de00 + de10 + de21) +  # 0, 1, 0, 0, 1
            0.25 * 0.75 * 0.45 * 0.05 * 0.45 * 0.05 * e00 * e11 * e20 * (de00 + de11 + de20) +  # 0, 1, 0, 1, 0
            0.25 * 0.75 * 0.45 * 0.05 * 0.45 * 0.45 * e00 * e11 * e21 * (de00 + de11 + de21) +  # 0, 1, 0, 1, 1
            0.25 * 0.75 * 0.05 * 0.45 * 0.05 * 0.05 * e01 * e10 * e20 * (de01 + de10 + de20) +  # 0, 1, 1, 0, 0
            0.25 * 0.75 * 0.05 * 0.45 * 0.05 * 0.45 * e01 * e10 * e21 * (de01 + de10 + de21) +  # 0, 1, 1, 0, 1
            0.25 * 0.75 * 0.05 * 0.05 * 0.45 * 0.05 * e01 * e11 * e20 * (de01 + de11 + de20) +  # 0, 1, 1, 1, 0
            0.25 * 0.75 * 0.05 * 0.05 * 0.45 * 0.45 * e01 * e11 * e21 * (de01 + de11 + de21) +  # 0, 1, 1, 1, 1

            0.75 * 0.25 * 0.05 * 0.05 * 0.45 * 0.45 * e00 * e10 * e20 * (de00 + de10 + de20) +  # 1, 0, 0, 0, 0
            0.75 * 0.25 * 0.05 * 0.05 * 0.45 * 0.05 * e00 * e10 * e21 * (de00 + de10 + de21) +  # 1, 0, 0, 0, 1
            0.75 * 0.25 * 0.05 * 0.45 * 0.05 * 0.45 * e00 * e11 * e20 * (de00 + de11 + de20) +  # 1, 0, 0, 1, 0
            0.75 * 0.25 * 0.05 * 0.45 * 0.05 * 0.05 * e00 * e11 * e21 * (de00 + de11 + de21) +  # 1, 0, 0, 1, 1
            0.75 * 0.25 * 0.45 * 0.05 * 0.45 * 0.45 * e01 * e10 * e20 * (de01 + de10 + de20) +  # 1, 0, 1, 0, 0
            0.75 * 0.25 * 0.45 * 0.05 * 0.45 * 0.05 * e01 * e10 * e21 * (de01 + de10 + de21) +  # 1, 0, 1, 0, 1
            0.75 * 0.25 * 0.45 * 0.45 * 0.05 * 0.45 * e01 * e11 * e20 * (de01 + de11 + de20) +  # 1, 0, 1, 1, 0
            0.75 * 0.25 * 0.45 * 0.45 * 0.05 * 0.05 * e01 * e11 * e21 * (de01 + de11 + de21) +  # 1, 0, 1, 1, 1

            0.75 * 0.75 * 0.05 * 0.05 * 0.05 * 0.05 * e00 * e10 * e20 * (de00 + de10 + de20) +  # 1, 1, 0, 0, 0
            0.75 * 0.75 * 0.05 * 0.05 * 0.05 * 0.45 * e00 * e10 * e21 * (de00 + de10 + de21) +  # 1, 1, 0, 0, 1
            0.75 * 0.75 * 0.05 * 0.45 * 0.45 * 0.05 * e00 * e11 * e20 * (de00 + de11 + de20) +  # 1, 1, 0, 1, 0
            0.75 * 0.75 * 0.05 * 0.45 * 0.45 * 0.45 * e00 * e11 * e21 * (de00 + de11 + de21) +  # 1, 1, 0, 1, 1
            0.75 * 0.75 * 0.45 * 0.05 * 0.05 * 0.05 * e01 * e10 * e20 * (de01 + de10 + de20) +  # 1, 1, 1, 0, 0
            0.75 * 0.75 * 0.45 * 0.05 * 0.05 * 0.45 * e01 * e10 * e21 * (de01 + de10 + de21) +  # 1, 1, 1, 0, 1
            0.75 * 0.75 * 0.45 * 0.45 * 0.45 * 0.05 * e01 * e11 * e20 * (de01 + de11 + de20) +  # 1, 1, 1, 1, 0
            0.75 * 0.75 * 0.45 * 0.45 * 0.45 * 0.45 * e01 * e11 * e21 * (de01 + de11 + de21)    # 1, 1, 1, 1, 1
        ) / expected_value
        np.testing.assert_array_almost_equal(jac, expected_jac)
