
Examples
========

The following documents illustrate the use of `cimem` using examples.

.. toctree::
    :maxdepth: 2

    cimem-without-connections
    cimem-with-parcels.rst
