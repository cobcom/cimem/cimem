.. _simple-cimem:

Using `cimem` without connectivity information
==============================================

The novelty of `cimem` is that it can make use of connectivity information to
solve the M/EEG inverse problem and provide a measure of information flow in
the white matter.
However, `cimem` can also be used if connectivity is not available, in which
case it will assume independence across time samples.
Because this simplifies the use of `cimem`, this first example will illustrate
how `cimem` can be used to without connectivity information to obtain source
estimates.

This example uses randomly generated data. To use it with your data will need
the following:

- **M/EEG data**: A `numpy` array with a shape of (M, T) where M is the number of
  sensors and T is the number of time samples.
- **Forward operator/gain matrix/leadfield matrix**: A `numpy` array with a
  shape of (M, N) where M is the number of sensors and N is the number of
  sources.

First, we import `cimem` and `numpy`. We also import `matplotlib` (it is not
a `cimem` dependency and may need to be installed) to visualize the results.

.. testcode::

    import matplotlib.pyplot as plt
    import numpy as np
    import cimem

Next we generate some random data. This is the part to modify if you wish to
use your own data.

.. testcode::

    # Purely random forward operator and source space. If you use your own
    # data, source_intensities will of course not be available.
    forward = np.random.randn(32, 100)
    source_intensities = np.random.randn(100, 1)
    data = np.dot(forward, source_intensities)

    nb_sensors, nb_sources = forward.shape
    _, nb_samples = source_intensities.shape

`cimem` is based around the idea of states and priors associated to clusters.
For this example, we will assume all sources are independent and share a single
state with normally distributed source intensities.
The mean is described by a `numpy` array with a shape of (Nc,) where Nc is the
number of sources of the cluster. In our case, there is a single cluster that
covers the entire cortex, therefore Nc == N.
The variance is described by a `numpy` array with a shape of (Nc, Nc).
The leadfields of the sources in the cluster must also be provided.
Because the cluster spans the entire brain, here we simply provide the full
forward operator.

.. testcode::

    mean = np.zeros((nb_sources,))
    variance = np.eye(nb_sources, nb_sources)
    priors = [cimem.GaussianPrior(mean, variance, forward)]

    # We create a single cluster that contains all sources with the priors
    # above.
    clusters = [cimem.Cluster('all-sources', range(nb_sources), priors, s)
                for s in range(nb_samples)]

Once the priors and clusters are setup, all that is left is solve the `cimem`
problem and estimate source intensities from the results.

.. testcode::

    lagrange, marginals = cimem.solve(data, clusters)
    recovered_source_intensities = cimem.reconstruct_source_intensities(
        marginals, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

Now let us visualize the results.
First, we check that the source intensities explain the data.

.. testcode::

    plt.figure(figsize=(10, 5), dpi=150)
    plt.plot(data, 'k', label='simulated')
    plt.plot(np.dot(forward, recovered_source_intensities), 'r', label='cimem recovered')
    plt.legend()
    plt.xlabel('sensors')
    plt.ylabel('amplitudes')
    plt.show()

The red and black lines should overlap, indicating that the recovered source
intensities indeed explains the simulated data.
We now compare the recovered source intensities to the simulated ones.
Under the simplified conditions used in this example (no connectivity, a single
cluster with a single Gaussian prior), the `cimem` solution can be shown to be
equivalent to the least squares solution.
We verify this by plotting the source estimates obtained using a
pseudo-inverse.

.. testcode::

    plt.figure(figsize=(10, 5), dpi=150)
    plt.plot(source_intensities, 'k', label='simulated')
    plt.plot(recovered_source_intensities, 'r', label='cimem recovered')
    plt.plot(np.dot(np.linalg.pinv(forward), data), '.b', label='pinv recovered')
    plt.legend()
    plt.xlabel('sources')
    plt.ylabel('intensities')
    plt.show()

Here, the red and black lines do not overlap indicating that `cimem` did not
recover the source intensities perfectly.
However, note that both source intensities explain the observed data and are
indistinguishable from a data fit perspective.
This is expected and is a direct consequence of the ill-posed nature of the
inverse problem.
Finally, we see that the blue dots match the red line, indicating that the
`cimem` solution matches the least square solution, validating the `cimem`
implementation.