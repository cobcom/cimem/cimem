.. _simple-cimem-parcellation:

Using `cimem` with a brain parcellation
=======================================

In the previous :ref:`example <simple-cimem>`, `cimem` was used without
connectivity information and with a single cluster that covered there entire
brain.
Here we add a bit of complexity by including a brain parcellation which will
add spatial regularization to the inverse problem.

This example uses randomly generated data. To use it with your data will need
the following:

- **M/EEG data**: A `numpy` array with a shape of (M, T) where M is the number of
  sensors and T is the number of time samples.
- **Forward operator/gain matrix/leadfield matrix**: A `numpy` array with a
  shape of (M, N) where M is the number of sensors and N is the number of
  sources.
- **Brain parcellation**: A `numpy` array of integers with a shape of (N,)
  where each entry is the label of a source.

First, we import `cimem` and `numpy`. We also import `matplotlib` (it is not
a `cimem` dependency and may need to be installed) to visualize the results.

.. testcode::

    import matplotlib.pyplot as plt
    import numpy as np
    import cimem

Next we generate some random data. This is the part to modify if you wish to
use your own data.

.. testcode::

    # Purely random forward operator.
    forward = np.random.randn(32, 100)

    nb_sensors, nb_sources = forward.shape

    # Generate the parcellation by generating a random label for each source.
    # We use 10 regions.
    labels = np.random.randint(10, size=(nb_sources,))

    # Generate random source intensities. Change the mean for one cluster. Of
    # course, if you use your own data, the source intensities will not be
    # available.
    source_intensities = np.random.randn(100, 1)
    source_intensities[labels == 0, :] += 10
    _, nb_samples = source_intensities.shape

    data = np.dot(forward, source_intensities)

For each of the regions of the parcellation, we create a cluster with two
possible states 0 (off) and 1 (on).

.. testcode::


    clusters = []
    for label in np.unique(labels):

        # Find the sources with the current label and the columns of the
        # forward operator corresponding to those sources.
        source_ids = np.nonzero(label == labels)[0]
        cluster_forward = forward[:, source_ids]

        # All clusters share the same mean and variance, but this does not have
        # to be the case.
        cluster_nb_sources = len(source_ids)
        off_mean = np.zeros((cluster_nb_sources,))
        on_mean = np.full((cluster_nb_sources,), 10.0)
        variance = np.eye(cluster_nb_sources, cluster_nb_sources)

        priors = [
            cimem.GaussianPrior(off_mean, variance, cluster_forward),  # off
            cimem.GaussianPrior(on_mean, variance, cluster_forward),   # on
        ]
        clusters.extend([
            cimem.Cluster(f'Cluster {label}', source_ids, priors, s)
            for s in range(nb_samples)])

Once the priors and clusters are setup, all that is left is solve the `cimem`
problem and estimate source intensities from the results.

.. testcode::

    lagrange, cost = cimem.solve(data, clusters, gradient_tolerance=1e-2)
    recovered_source_intensities = cimem.reconstruct_source_intensities(
        cost, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

First, it is always a good idea to check that the source intensities explain
the data. We expect a perfect match here because no noise was added.

.. testcode::

    plt.figure(figsize=(10, 5), dpi=150)
    plt.stem(data, 'k', label='simulated', markerfmt='ko', basefmt='k')
    plt.stem(
        np.dot(forward, recovered_source_intensities), 'r',
        label='cimem recovered', markerfmt='r.', basefmt='r')
    plt.legend()
    plt.xlabel('sensors')
    plt.ylabel('amplitudes')
    plt.show()

Next we visualize the reconstructed source intensities. A perfect agreement is
not expected here, because the problem is ill-posed. However, the intensities
of the sources that belong to the cluster whose mean was shifted should be
higher. We also compare the results to those of the pseudoinverse.

.. testcode::

    plt.figure(figsize=(10, 5), dpi=150)
    plt.stem(
        source_intensities, label='simulated', markerfmt='ko',
        basefmt='k')
    plt.stem(
        np.dot(np.linalg.pinv(forward), data), label='pinv recovered',
        markerfmt='b.', basefmt='b')
    plt.stem(
        recovered_source_intensities, 'r', label='cimem recovered',
        markerfmt='r.', basefmt='r')
    plt.legend()
    plt.xlabel('sources')
    plt.ylabel('intensities')
    plt.show()

Finally, we can also visualise the posterior probability of each cluster. It
should be clearly visible which cluster was given a higher mean.

.. testcode::

    # Here we only query the probability of being in state 1 (on) for each
    # cluster.
    cluster_posteriors = [cost.marginals[c].probabilities[1] for c in clusters]

    plt.figure(figsize=(10, 5), dpi=150)
    plt.stem(cluster_posteriors, 'k', markerfmt='ko', basefmt='k')
    plt.ylabel('posterior active probability')
    plt.xticks(np.arange(len(clusters)), [c.name for c in clusters], rotation=90)
    plt.ylim((0, 1.1))
    plt.show()
