
=================================
Welcome to cimem's documentation!
=================================

A Python 3 package package that implements Connectivity Informed Maximum Entropy
on the Mean (`Deslauriers-Gauthier et al., 2019`_).

User documentation
==================

.. toctree::
    :maxdepth: 2

    installation
    examples/index


.. _Deslauriers-Gauthier et al., 2019:
    https://www.sciencedirect.com/science/article/pii/S1053811919305981
