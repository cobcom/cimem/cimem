# cimem
A Python 3 package package that implements Connectivity Informed Maximum Entropy
on the Mean ([Deslauriers-Gauthier et al., 2019][1]).

## Installation

For now, most people are expected to use `cimem` as developers and should
 install it from the source. We strongly recommend installing `cimem` in a
 conda (https://docs.conda.io/en/latest/miniconda.html) or virtualenv
 environment. Note that `cimem` is Python 3 only, so your environment should
 set the interpreter accordingly. For example, if you have installed miniconda
 from the link above, you can setup and activate your environment by running:
 ```bash
 conda create -n cimem python=3
 conda activate cimem
 ```

The `cimem` package relies on the `recur` and `bayesnet` packages which we
 also recommend you install from their respective source by running:
 ```bash
 git clone https://gitlab.inria.fr/cobcom/cimem/recur.git
 cd recur
 python setup.py develop
 cd ..

 git clone https://gitlab.inria.fr/cobcom/cimem/bayesnet.git
 cd bayesnet
 git checkout devel
 python setup.py develop
 cd ..
 ```

With these dependencies installed, `cimem` can be installed from the source
by running:
```bash
git clone https://gitlab.inria.fr/cobcom/cimem/cimem.git
cd cimem
python setup.py develop
```

To make sure everything was installed correctly, run the tests with:
```bash
python -m unittest
```
The output should indicate that a number of tests have passed.

## Documentation

Generating the documentation requires the `sphinx` package. It can be installed
in your conda environment by running: 
```bash
conda install sphinx
``` 
The documentation can then be compiled by moving into the `cimem/doc` directory
and running:
```bash
make html
```
The root of the documentation is `cimem/doc/build/html/index.html`.

## Quickstart

Coming soon!


[1]: https://www.sciencedirect.com/science/article/pii/S1053811919305981
