from setuptools import setup

setup(
    name='cimem',
    version='0.0.1',
    packages=['cimem'],
    scripts=['scripts/cimem'],
    url='https://github.com/sdeslauriers/cimem',
    license='MIT',
    author='Samuel Deslauriers-Gauthier',
    author_email='sam.deslauriers@gmail.com',
    description='A Python package that implements Connectivity Informed '
                'Maximum Entropy on the Mean (CIMEM)',
    install_requires=['numpy', 'bayesnet-marginals', 'scipy']
)
