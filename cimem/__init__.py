import json
import logging.config
import os

from typing import Callable
from typing import Sequence
from typing import Tuple

import numpy as np

import bayesnet
import scipy.sparse as sp
from scipy.optimize import minimize
from bayesnet import pmf

from .core import Cluster
from .core import GaussianPrior
from .typing import Matrix


# Load logging configuration.
config_file = os.path.join(os.path.dirname(__file__),
                           '..', 'config', 'logging.json')
logging.config.dictConfig(json.load(open(config_file, 'rt')))
logger = logging.getLogger(__name__)


def solve(data: np.ndarray, clusters: Sequence[Cluster],
          pmfs: Sequence[bayesnet.AbstractProbabilityMassFunction] = None,
          covariance: Matrix = None,
          gradient_tolerance: float = 1e-6) \
        -> Tuple[np.ndarray, Callable]:
    """Solves the inverse problem using CIMEM

    Solves the inverse problem using Connectivity Informed Maximum Entropy
    on the Mean (CIMEM).

    Args:
        data: The M/EEG data in a 1D numpy array of floats.
        clusters: The source clusters used to spatially regularize the problem.
        pmfs: Other probability mass table to take into account when fitting
            the data. This is where additional priors are included into the
            problem.
        covariance: The noise covariance of the data with a shape of
            (data.size, data.size). If not provided, defaults to an all zero
            matrix.
        gradient_tolerance: The gradient norm tolerance for termination.

    Returns:
        lagrange: The optimal Lagrange multipliers.
        marginals: The marginals of the Bayesian network used to define the
            priors.

    """

    if data.ndim == 1:
        data = data[:, None]

    if pmfs is None:
        pmfs = []

    if covariance is None:
        covariance = sp.csr_matrix((data.size, data.size), dtype=np.float)

    # Build the cost function based on the clusters and pmfs.
    cost = Cost(data, clusters, pmfs, covariance)

    logger.info('Solving CIMEM optimization problem.')
    logger.debug(f'             Entropy       Gradient norm')
    logger.debug(f'----------------------------------------')
    res = minimize(cost, np.zeros((data.size,)),
                   jac=cost.jac,
                   options={'gtol': gradient_tolerance},
                   method='CG')

    # Warn the user if the solver did not terminate successfully.
    if not res.success:
        logger.warning(f'{res.message}')
    else:
        logger.info(f'{res.message}')

    return res.x, cost


def reconstruct_source_intensities(
        cost, clusters: Sequence[Cluster],
        nb_sensors: int, nb_sources: int, nb_samples: int,
        lagrange: np.ndarray) -> np.ndarray:
    """Reconstructs the source intensities

    Reconstructs the source intensities for a given value of the Lagrange
    multipliers. If the Lagrange multipliers used are those returned by the
    solve function, this corresponds to the CIMEM source estimations.

    Args:
        cost: The cost function that was minimized.
        clusters: The clusters used to spatially regularize the problem.
        nb_sensors: The number of sensors of the data.
        nb_sources: The number of sources of the model.
        nb_samples: The number of samples of the data.
        lagrange: The Lagrange multipliers where the sources are
            reconstructed. Usually those returned by the solve function.

    Returns:
        intensities: The reconstructed source intensities.

    """

    # Evaluate the cost to update the marginals.
    cost(lagrange)

    # Initialize the source intensities.
    intensities = np.zeros((nb_sources, nb_samples))

    sensors = np.arange(nb_sensors)
    for cluster in clusters:

        # Compute the posterior of the cluster.
        posterior = cost.marginals[cluster].probabilities

        # Compute the source intensities of the cluster.
        for i, prior in enumerate(cluster.priors):
            location = sensors + nb_sensors * cluster.sample
            prior_intensities = prior.derivative(lagrange[location])
            intensities[cluster.sources, cluster.sample] += \
                posterior[i] * prior_intensities

    return intensities


class Cost(object):

    def __init__(self, data, clusters, pmfs, covariance):
        """Represents the CIMEM cost function which should be minimized"""

        # Flatten the data.
        nb_sensors, nb_samples = data.shape
        flat_data = data.ravel('F')
        sensors = np.arange(nb_sensors)

        # For each cluster, add an evidence table.
        evidence_tables = [pmf([c]) for c in clusters]
        nb_states = np.sum([len(c) for c in clusters])

        # Compute the marginals.
        logger.info('Computing marginals.')
        marginals = bayesnet.marginals(pmfs + evidence_tables)

        # Save precomputed results.
        self.nb_sensors = nb_sensors
        self.flat_data = flat_data
        self.sensors = sensors
        self.nb_states = nb_states
        self.marginals = marginals
        self.nb_states = nb_states

        self.clusters = clusters
        self.evidence_tables = evidence_tables
        self.covariance = covariance

        # Reserve space for the gradients and locations once.
        self.gradients = np.zeros((nb_states, nb_sensors))
        self.locations = np.zeros((nb_states, nb_sensors), dtype=int)

        self._lagrange = None

    def _update_marginals(self, lagrange):
        """Update the marginals for the specified Lagrange multipliers"""

        # If the Lagrange vector is the same one we got previously, the
        # marginals are already updated.
        if np.array_equal(self._lagrange, lagrange):
            return

        nb_sensors = self.nb_sensors
        sensors = self.sensors
        clusters = self.clusters
        evidence_tables = self.evidence_tables
        marginals = self.marginals
        gradients = self.gradients
        locations = self.locations

        # Update the evidence and marginals for the current Lagrange
        # multipliers.
        i = 0
        for cluster, evidence_table in zip(clusters, evidence_tables):
            location = sensors + nb_sensors * cluster.sample
            values, gradient = zip(*(p.data_partition(lagrange[location])
                                     for p in cluster.priors))

            if any(np.isinf(values)):
                raise ValueError(
                    f'The entropy cannot be evaluated for this value of the '
                    f'Lagrange multiplier. This is usually caused by a priors '
                    f'that are too unlikely. Increasing the noise covariance '
                    f'may help.')

            gradients[i:i + len(cluster), :] = gradient
            locations[i:i + len(cluster), :] = location
            i += len(cluster)

            normalization = np.sum(values)
            evidence_table.probabilities = values / normalization
            evidence_table.normalization = normalization

        marginals.update()

        self._lagrange = lagrange

    def __call__(self, lagrange):
        """Evaluate the cost function"""

        self._update_marginals(lagrange)

        # Compute the MEM cost.
        temp = self.covariance.dot(lagrange)
        flat_data = self.flat_data
        entropy = 0.5 * np.dot(lagrange, temp) - np.dot(lagrange, flat_data)

        # Because the normalization coefficient of independent marginals is
        # the product of their respective normalization coefficients, it is
        # numerically much better to compute the sum of the log rather than
        # the log of the product. This is what log_normalization does.
        entropy += self.marginals.log_normalization

        logger.debug(f'{entropy:20.8f}                    ')

        return entropy

    def jac(self, lagrange):
        """Evaluate the Jacobian of the cost function"""

        self._update_marginals(lagrange)

        clusters = self.clusters
        marginals = self.marginals
        gradients = self.gradients
        locations = self.locations

        weights = [p for c in clusters for p in marginals[c].probabilities]
        weighted_gradients = np.array(weights)[:, None] * gradients
        gradient = self.covariance.dot(lagrange) - self.flat_data
        for location, cluster_gradient in zip(locations, weighted_gradients):
            gradient[location] += cluster_gradient

        logger.debug(f'                    {np.linalg.norm(gradient):20.8f}')

        return gradient
