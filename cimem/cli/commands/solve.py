import logging
import pickle
import os

import numpy as np
import scipy.sparse as sp

import cimem


def add_parser(subparsers):

    # The filter subparser.
    solve_subparser = subparsers.add_parser(
        'solve',
        description='Solves the inverse problem using CIMEM and provides '
                    'sources estimates, connectivity information, etc.',
        help='Solve inverse problem using CIMEM')
    solve_subparser.add_argument(
        'forward_filename', metavar='<forward>', type=str,
        help='The file that contains the forward operator. For now, only '
             'the .npy format is supported.')
    solve_subparser.add_argument(
        'data_filename', metavar='<data>', type=str,
        help='The file that contains the M/EEG data. For now, only the '
             '.npy format is supported.')
    solve_subparser.add_argument(
        'output_filename', metavar='<output>', type=str,
        help='The file where the results will be saved. The file format '
             'is a pickle, regardless of the file extension.')
    solve_subparser.add_argument(
        '--labels', metavar='<labels>', dest='labels_filename',
        type=str, default=None,
        help='The file that contains the source labels. For now, only '
             'the .npy format is supported. This file must contain a 1D '
             'array where each entry is the label of a source.')
    solve_subparser.add_argument(
        '--force', '-f', action='store_true',
        help='Force overwriting the output file.')
    solve_subparser.add_argument(
        '--verbose', '-v', action='store_true',
        help='Make output more verbose.')

    solve_subparser.set_defaults(func=solve)


def solve(
        forward_filename: str,
        data_filename: str,
        output_filename: str,
        labels_filename: str,
        force: bool,
        verbose: bool):
    """Solves the inverse problem using CIMEM

    Solves the inverse problem using CIMEM and provides source estimates,
    connectivity information, regions states, entropy, etc.

    Args:
        forward_filename: The file that contains the forward operator. Only the
            .npy format is supported.
        data_filename: The file that contains the M/EEG data. Only the .npy
            format is supported.
        output_filename: The file where the results will be saved.
        labels_filename: The file that contains the labels for each source.
            Only the .npy format is supported.
        force: Force overwriting the output file if it exists.
        verbose: Make output more verbose.

    """

    # If the output file exists.
    if os.path.exists(output_filename) and not force:
        raise FileExistsError(
            f'The output file already exists. Use --force to overwrite it.')

    if verbose:
        cimem.logger.setLevel(logging.DEBUG)

    forward = np.load(forward_filename)
    data = np.load(data_filename)

    # Make sure the dimensions are consistent.
    nb_sensors, nb_sources = forward.shape
    _, nb_samples = data.shape

    if data.shape[0] != nb_sensors:
        raise ValueError(
            f'The number of sensors of the forward operator and data are not '
            f'consistent ({nb_sensors} != {data.shape[0]}).')

    # Scale the data and the forward operator to have better numerical
    # precision. Scale the data by 1e9 to have the current in nA.m. Then, scale
    # both the forward operator and the data to get the data in the [-1, 1]
    # range.
    data *= 1e9
    normalization = np.max(np.abs(data))
    data /= normalization
    forward /= normalization

    # If the labels were not specified, using a single label for all sources.
    if labels_filename is None:
        labels = np.ones((nb_sources,))
    else:
        labels = np.load(labels_filename)
        if labels.ndim != 1 or len(labels) != nb_sources:
            raise ValueError(
                f'The size of the cluster labels does not match the number '
                f'of sources.')

    # Build the clusters based on input labels.
    clusters = []
    for label in np.unique(labels):

        # Find the sources with the current label and the columns of the
        # forward operator corresponding to those sources.
        source_ids = np.nonzero(label == labels)[0]
        cluster_forward = forward[:, source_ids]

        # All clusters share the same mean and variance. The mean is set to
        # 10 nA.m according to empirical observations and invasive studies.
        cluster_nb_sources = len(source_ids)
        off_mean = np.zeros((cluster_nb_sources,))
        on_mean = np.full((cluster_nb_sources,), 10.0)
        variance = sp.eye(cluster_nb_sources, cluster_nb_sources) * 10.0

        priors = [
            cimem.GaussianPrior(off_mean, variance, cluster_forward),
            cimem.GaussianPrior(on_mean, variance, cluster_forward),
            cimem.GaussianPrior(-on_mean, variance, cluster_forward),
        ]
        clusters.extend([
            cimem.Cluster(f'Cluster {label}', source_ids, priors, s)
            for s in range(nb_samples)])

    # By default, we assume all clusters are off.
    pmfs = [cimem.pmf([c], [0.98, 0.01, 0.01]) for c in clusters]

    # Use a default noise covariance matrix assuming iid Gaussian noise.
    covariance = sp.eye(data.size, data.size) * 1e-2

    lagrange, cost = cimem.solve(
        data, clusters, pmfs=pmfs, covariance=covariance,
        gradient_tolerance=1e-5 * np.sqrt(data.size))
    recovered_source_intensities = cimem.reconstruct_source_intensities(
        cost, clusters, nb_sensors, nb_sources, nb_samples, lagrange)

    # Save the results to a pickle.
    results = {
        'nb_source': nb_sources,
        'nb_sensors': nb_sensors,
        'nb_samples': nb_samples,
        'nb_clusters': len(np.unique(labels)),
        'lagrange': lagrange,
        'cost': cost,
        'clusters': clusters,
        'source_intensities': recovered_source_intensities * 1e-9,
        'data': data * normalization * 1e-9,
        'forward': forward * normalization,
        'normalization': normalization,
    }

    with open(output_filename, 'wb') as f:
        pickle.dump(results, f)
