import abc
import typing

import numpy as np
import scipy.sparse


class MatrixLike(abc.ABC):
    """Defines the requirements for a MatrixLike object

    Any object having ndim, shape, dot and T in its __dict__ is considered
    a MatrixLike.

    """

    @abc.abstractmethod
    def dot(self, other) -> 'Matrix':
        pass

    @property
    @abc.abstractmethod
    def ndim(self) -> int:
        pass

    @property
    @abc.abstractmethod
    def shape(self) -> typing.Sequence:
        pass

    @abc.abstractmethod
    def transpose(self) -> 'Matrix':
        pass

    @classmethod
    def __subclasshook__(cls, obj):

        if cls is MatrixLike:
            required = ('dot', 'transpose', 'ndim', 'shape')
            return all(_in_dict(obj, i) for i in required)

        return NotImplemented


def _in_dict(obj, item):
    if any(item in sub.__dict__ for sub in obj.__mro__):
        return True
    return False


# Numpy arrays and scipy sparse matrices are matrix like, but we
# still specify them here to help static analysis by IDEs.
Matrix = typing.Union[MatrixLike, np.ndarray, scipy.sparse.spmatrix]
